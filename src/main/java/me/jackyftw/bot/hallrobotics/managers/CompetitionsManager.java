package me.jackyftw.bot.hallrobotics.managers;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import me.jackyftw.bot.hallrobotics.HallRoboticsBot;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.TextChannel;

import javax.net.ssl.HttpsURLConnection;
import java.awt.Color;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class CompetitionsManager {

	private static final int SEASON_ID = 173;
	private static final int NUM_TEAMS = 2;

	public static void init() {
		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				updateMessage("1697A", NUM_TEAMS - 1);
				updateMessage("1697B", NUM_TEAMS);

				HallRoboticsBot.log("Updated upcoming competitions");
			}
		}, 0L, TimeUnit.HOURS.toMillis(1));
	}

	private static void updateMessage(String teamNumber, int position) {
		try {
			// Get all json data for the team
			int teamId = getTeamId(teamNumber);
			if(teamId == -1) {
				HallRoboticsBot.log("Error retrieving team data for " + teamNumber);
				return;
			}
			JsonObject latestEvent = getLatestEvent(teamId);

			// Get embed
			MessageEmbed embed = getEmbed(teamNumber, latestEvent);

			// Send embed
			TextChannel channel = HallRoboticsBot.getConfig().getCompetitionsChannel();
			List<Message> msgs = channel.getHistory().retrievePast(2).complete();

			if(msgs.size() == NUM_TEAMS) {
				// Edit embed
				Message msg = msgs.get(msgs.size() - position);
				msg.editMessageEmbeds(embed).queue();
			} else {
				// Send embed for first time
				channel.sendMessageEmbeds(embed).queue();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static MessageEmbed getEmbed(String teamNumber, JsonObject latestEvent) {
		EmbedBuilder builder = new EmbedBuilder().setTitle("Upcoming Competition for "  + teamNumber);
		OffsetDateTime now = OffsetDateTime.now();

		if(latestEvent != null) {
			Date eventStart = getStartTime(latestEvent);

			if(eventStart != null) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(eventStart);

				if(now.toInstant().isBefore(eventStart.toInstant())) {
					JsonObject locData = latestEvent.getAsJsonObject("location");

					// Is upcoming
					return builder.setColor(Color.BLUE)
							.addField("Name", latestEvent.getAsJsonPrimitive("name").getAsString(), false)
							.addField("Venue", locData.getAsJsonPrimitive("venue").getAsString(), false)
							.addField("Location", locData.getAsJsonPrimitive("city").getAsString() + ", " +
									locData.getAsJsonPrimitive("region").getAsString(), false)
							.addField("Date", cal.get(Calendar.MONTH) + 1 + "/" + cal.get(Calendar.DAY_OF_MONTH) + "/" + cal.get(Calendar.YEAR), false)
							.setFooter("Updated")
							.setTimestamp(now)
							.build();
				}
			}
		}

		return builder.setColor(Color.RED)
					.addField(":x:", "No upcoming competition", false)
					.setFooter("Updated")
					.setTimestamp(now)
					.build();
	}

	private static int getTeamId(String teamNumber) throws IOException {
		URL url = new URL("https://www.robotevents.com/api/v2/teams?number[]=" + teamNumber);
		HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");
		conn.setRequestProperty("Authorization", "Bearer " + HallRoboticsBot.getConfig().getRobotEventsAPIKey());

		int status = conn.getResponseCode();
		if(status != 200) return -1;

		return JsonParser.parseReader(new InputStreamReader(conn.getInputStream())).getAsJsonObject()
				.getAsJsonArray("data").get(0)
				.getAsJsonObject().getAsJsonPrimitive("id")
				.getAsInt();
	}

	private static JsonObject getLatestEvent(int teamNumber) throws IOException {
		URL url = new URL("https://www.robotevents.com/api/v2/teams/" + teamNumber + "/events?season[]=" + SEASON_ID);
		HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");
		conn.setRequestProperty("Authorization", "Bearer " + HallRoboticsBot.getConfig().getRobotEventsAPIKey());

		int status = conn.getResponseCode();
		if(status != 200) return null;

		JsonArray events = JsonParser.parseReader(new InputStreamReader(conn.getInputStream())).getAsJsonObject().getAsJsonArray("data");

		JsonObject latestEvent = null;
		for(JsonElement eventElement : events) {
			JsonObject event = eventElement.getAsJsonObject();
			long start = getStartTime(event).toInstant().toEpochMilli();

			if(start <= System.currentTimeMillis()) continue;

			if(latestEvent == null) latestEvent = event;
			if(start <= getStartTime(latestEvent).getTime()) latestEvent = event;
		}

		return latestEvent;
	}

	private static Date getStartTime(JsonObject event) {
		String start = event.getAsJsonPrimitive("start").getAsString();
		try {
			return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX").parse(start);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return null;
	}

}
