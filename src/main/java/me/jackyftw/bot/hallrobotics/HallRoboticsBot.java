package me.jackyftw.bot.hallrobotics;

import me.jackyftw.bot.hallrobotics.command.CommandHandler;
import me.jackyftw.bot.hallrobotics.file.Config;
import me.jackyftw.bot.hallrobotics.listeners.GeneralListener;
import me.jackyftw.bot.hallrobotics.managers.CompetitionsManager;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.MemberCachePolicy;

import javax.security.auth.login.LoginException;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class HallRoboticsBot {

	private static Config config;
	private static JDA jda;

	public static void main(String[] args) {
		// Load files
		try {
			config = new Config().loadConfig();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		log("Loaded config file");

		// Start bot
		startBot();

		// Shutdown hook
		Runtime.getRuntime().addShutdownHook(new Thread(HallRoboticsBot::shutdown));

		// Listen for stop
		new Thread(() -> {
			Scanner in = new Scanner(System.in);
			while(in.hasNextLine()) {
				if(in.nextLine().equalsIgnoreCase("stop")) {
					System.exit(0);
					break;
				}
			}
		}).start();
	}

	private static void startBot() {
		try {
			jda = JDABuilder.createDefault(config.getBotToken(), GatewayIntent.getIntents(GatewayIntent.ALL_INTENTS))
					.setActivity(Activity.playing("with VEX robots"))
					.setMemberCachePolicy(MemberCachePolicy.ALL)
					.addEventListeners(new GeneralListener(), new CommandHandler())
					.enableIntents(GatewayIntent.GUILD_MEMBERS, GatewayIntent.GUILD_MESSAGES)
					.build()
					.awaitReady();
		} catch (LoginException | InterruptedException e) {
			e.printStackTrace();
		}

		log("Bot connected successfully");

		CommandHandler.init();
		CompetitionsManager.init();

		log("Managers initialized");
	}

	private static void shutdown() {
		log("Shutdown hook called");

		jda.shutdown();
		log("Bot disconnected");
	}

	public static void log(String msg) {
		SimpleDateFormat format = new SimpleDateFormat("'['MM-dd HH:mm:ss.S']' ");
		String prefix = format.format(new Date());

		System.out.println(prefix + msg);
	}

	public static Guild getGuild() {
		return jda.getGuildById("680219340712312862");
	}

	public static Config getConfig() {
		return config;
	}

}
