package me.jackyftw.bot.hallrobotics.file;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

public class GsonExclusionStrategy implements ExclusionStrategy {

	@Override
	public boolean shouldSkipField(FieldAttributes field) {
		return field.getName().equals("fileName");
	}

	@Override
	public boolean shouldSkipClass(Class<?> clazz) {
		return false;
	}

}
