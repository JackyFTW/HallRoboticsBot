package me.jackyftw.bot.hallrobotics.file;

import com.google.gson.Gson;
import me.jackyftw.bot.hallrobotics.HallRoboticsBot;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class Config extends FileLoader {

	private String botToken = "";
	private String robotEventsAPIKey = "";
	private String teamMembersRoleId = "";
	private String alumniRoleId = "";
	private String welcomeChannelId = "";
	private String botCommandsChannelId = "";
	private String competitionsChannelId = "";

	public Config() {
		super("config.json");
	}

	public Config loadConfig() throws FileNotFoundException {
		File configFile = loadFile();

		return new Gson().fromJson(new FileReader(configFile), getClass());
	}

	public String getBotToken() {
		return botToken;
	}

	public String getRobotEventsAPIKey() {
		return robotEventsAPIKey;
	}

	public Role getTeamMembersRole() {
		return HallRoboticsBot.getGuild().getRoleById(teamMembersRoleId);
	}

	public Role getAlumniRole() {
		return HallRoboticsBot.getGuild().getRoleById(alumniRoleId);
	}

	public TextChannel getWelcomeChannel() {
		return HallRoboticsBot.getGuild().getTextChannelById(welcomeChannelId);
	}

	public TextChannel getBotCommandsChannel() {
		return HallRoboticsBot.getGuild().getTextChannelById(botCommandsChannelId);
	}

	public TextChannel getCompetitionsChannel() {
		return HallRoboticsBot.getGuild().getTextChannelById(competitionsChannelId);
	}

}
