package me.jackyftw.bot.hallrobotics.file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

public abstract class FileLoader {

	private String fileName;

	public FileLoader(String fileName) {
		this.fileName = fileName;
	}

	public File loadFile() {
		File file = new File(fileName);
		if(file.exists()) return file;

		try {
			file.createNewFile();

			// Write empty properties
			BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/" + fileName)));
			BufferedWriter bw = new BufferedWriter(new FileWriter(file, true));

			br.lines().forEach(line -> {
				try {
					bw.write(line);
					bw.newLine();
				} catch (IOException e) {
					e.printStackTrace();
				}
			});

			bw.close();
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return file;
	}

}
