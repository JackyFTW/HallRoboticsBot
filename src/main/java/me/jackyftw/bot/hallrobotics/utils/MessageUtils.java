package me.jackyftw.bot.hallrobotics.utils;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;

public class MessageUtils {

	public static void sendMessageAndDelete(TextChannel channel, Message msg, long deleteAfterMillis) {
		new Thread(() -> {
			try {
				// Send message
				Message sentBotMsg = channel.sendMessage(msg).complete();

				// Delete it 20s later
				Thread.sleep(deleteAfterMillis);
				sentBotMsg.delete().queue();
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
		}).start();
	}

}
