package me.jackyftw.bot.hallrobotics.command;

import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;

public abstract class Command {

	private final String name;
	private final String usage;
	private final boolean adminsOnly;

	public Command(String name, String usage, boolean adminsOnly) {
		this.name = name;
		this.usage = usage;
		this.adminsOnly = adminsOnly;
	}

	public abstract boolean onCommand(TextChannel channel, Member member, String[] args);

	public String getName() {
		return name;
	}

	public String getUsage() {
		return usage;
	}

	public boolean isAdminsOnly() {
		return adminsOnly;
	}

}
