package me.jackyftw.bot.hallrobotics.command;

import me.jackyftw.bot.hallrobotics.HallRoboticsBot;
import me.jackyftw.bot.hallrobotics.command.cmds.GraduateCmd;
import me.jackyftw.bot.hallrobotics.command.cmds.InfoCmd;
import me.jackyftw.bot.hallrobotics.utils.MessageUtils;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class CommandHandler extends ListenerAdapter {

	private static final List<Command> commands = new ArrayList<>();

	public static void init() {
		commands.add(new InfoCmd());
		commands.add(new GraduateCmd());
	}

	private static Command getCommand(String name) {
		for(Command cmd : commands) {
			if(cmd.getName().equalsIgnoreCase(name)) return cmd;
		}

		return null;
	}

	@Override
	public void onMessageReceived(MessageReceivedEvent e) {
		if(!e.isFromGuild()) return;
		if(!e.isFromType(ChannelType.TEXT)) return;

		Member member = e.getMember();
		TextChannel channel = e.getTextChannel();
		String msg = e.getMessage().getContentRaw();
		TextChannel botCommandsChannel = HallRoboticsBot.getConfig().getBotCommandsChannel();
		assert member != null;

		if(!msg.startsWith("!")) return;
		e.getMessage().delete().queue();

		if(!channel.getId().equals(botCommandsChannel.getId())) {
			String botMsg = ":x: " + member.getAsMention() + ", please use " + botCommandsChannel.getAsMention() + " instead to run commands!";
			MessageUtils.sendMessageAndDelete(channel, new MessageBuilder(botMsg).build(), TimeUnit.SECONDS.toMillis(10));
		} else {
			String fullCommand = msg.substring(1).trim();
			String commandName = fullCommand.split(" ")[0];
			Command cmd = getCommand(commandName);

			if(cmd == null) {
				String botMsg = ":thinking: " + member.getAsMention() + ", I don't know that command...";
				MessageUtils.sendMessageAndDelete(botCommandsChannel, new MessageBuilder(botMsg).build(), TimeUnit.SECONDS.toMillis(10));
				return;
			} else if(cmd.isAdminsOnly() && !member.getRoles().contains(HallRoboticsBot.getGuild().getRoleById("680237403990130797"))) {
				String botMsg = ":stop_sign: " + member.getAsMention() + ", this command is limited to **administrators only**!";
				MessageUtils.sendMessageAndDelete(botCommandsChannel, new MessageBuilder(botMsg).build(), TimeUnit.SECONDS.toMillis(10));
				return;
			}

			String trimmedArgs = fullCommand.substring(fullCommand.indexOf(" ") + 1).trim();
			String[] args =
					fullCommand.contains(" ") ?
							(trimmedArgs.contains(" ") ?
									(trimmedArgs.split(" "))
									: new String[] { trimmedArgs })
							: new String[0];
			boolean hideUsage = cmd.onCommand(botCommandsChannel, member, args);

			if(!hideUsage) {
				String botMsg = ":x: " + member.getAsMention() + ", incorrect usage! **Usage:** !" + cmd.getUsage();
				MessageUtils.sendMessageAndDelete(botCommandsChannel, new MessageBuilder(botMsg).build(), TimeUnit.SECONDS.toMillis(10));
			}
		}
	}

}
