package me.jackyftw.bot.hallrobotics.command.cmds;

import me.jackyftw.bot.hallrobotics.HallRoboticsBot;
import me.jackyftw.bot.hallrobotics.command.Command;
import me.jackyftw.bot.hallrobotics.utils.MessageUtils;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;

import java.util.concurrent.TimeUnit;

public class InfoCmd extends Command {

	public InfoCmd() {
		super("info", "info <graduation year> <first name last initial>", false);
	}

	@Override
	public boolean onCommand(TextChannel channel, Member member, String[] args) {
		if(args.length != 3) return false;

		String graduationYear = args[0];
		String firstName = args[1];
		String lastInitial = args[2];

		// Verify args
		try {
			Integer.parseInt(graduationYear);

			if(lastInitial.length() != 1) {
				String botMsg = member.getAsMention() + ", invalid last initial! **Example:** 'B', 'D', etc";
				MessageUtils.sendMessageAndDelete(channel, new MessageBuilder(botMsg).build(), TimeUnit.SECONDS.toMillis(10));
				return true;
			}
		} catch(NumberFormatException ignored) {
			String botMsg = member.getAsMention() + ", invalid year supplied! **Example:** '2023', '2022', etc";
			MessageUtils.sendMessageAndDelete(channel, new MessageBuilder(botMsg).build(), TimeUnit.SECONDS.toMillis(10));
			return true;
		}

		if(HallRoboticsBot.getGuild().getSelfMember().canInteract(member)) {
			HallRoboticsBot.getGuild().modifyNickname(member, graduationYear + " | " + firstName + " " + lastInitial).queue();

			String botMsg = ":white_check_mark: " + member.getAsMention() + ", your information has been successfully updated!";
			MessageUtils.sendMessageAndDelete(channel, new MessageBuilder(botMsg).build(), TimeUnit.SECONDS.toMillis(10));
		} else {
			String botMsg = ":x: " + member.getAsMention() + ", I do not have permission to edit your information!";
			MessageUtils.sendMessageAndDelete(channel, new MessageBuilder(botMsg).build(), TimeUnit.SECONDS.toMillis(10));
		}

		return true;
	}

}
