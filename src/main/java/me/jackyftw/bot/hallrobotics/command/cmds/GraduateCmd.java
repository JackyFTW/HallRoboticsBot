package me.jackyftw.bot.hallrobotics.command.cmds;

import me.jackyftw.bot.hallrobotics.HallRoboticsBot;
import me.jackyftw.bot.hallrobotics.command.Command;
import me.jackyftw.bot.hallrobotics.utils.MessageUtils;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;

import java.util.concurrent.TimeUnit;

public class GraduateCmd extends Command {

	public GraduateCmd() {
		super("graduate", "graduate <graduation year>", true);
	}

	@Override
	public boolean onCommand(TextChannel channel, Member member, String[] args) {
		if(args.length != 1) return false;

		try {
			Integer.parseInt(args[0]);
		} catch(NumberFormatException e) {
			String botMsg = member.getAsMention() + ", invalid year supplied! **Example:** '2023', '2022', etc";
			MessageUtils.sendMessageAndDelete(channel, new MessageBuilder(botMsg).build(), TimeUnit.SECONDS.toMillis(10));
			return true;
		}

		int affectedMembers = 0;
		for(Member teamMember : HallRoboticsBot.getGuild().getMembersWithRoles(HallRoboticsBot.getConfig().getTeamMembersRole())) {
			String name = teamMember.getEffectiveName();

			if(name.startsWith(args[0])) {
				teamMember.getRoles().forEach(r -> HallRoboticsBot.getGuild().removeRoleFromMember(teamMember, r).queue());
				HallRoboticsBot.getGuild().addRoleToMember(teamMember, HallRoboticsBot.getConfig().getAlumniRole()).queue();
				HallRoboticsBot.getGuild().modifyNickname(teamMember, "Grad " + name.substring(name.indexOf("|"))).queue();

				affectedMembers++;
			}
		}

		String botMsg = ":white_check_mark: " + member.getAsMention() + ", successfully graduated **" + affectedMembers + "** team members!";
		MessageUtils.sendMessageAndDelete(channel, new MessageBuilder(botMsg).build(), TimeUnit.SECONDS.toMillis(10));
		return true;
	}

}
