package me.jackyftw.bot.hallrobotics.listeners;

import me.jackyftw.bot.hallrobotics.HallRoboticsBot;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class GeneralListener extends ListenerAdapter {

	@Override
	public void onGuildMemberJoin(GuildMemberJoinEvent e) {
		Member member = e.getMember();
		if(member.getUser().isBot()) return;

		String botMsg =
				 "@here Welcome " + member.getAsMention() + " to the server!\n" +
				"*Please read <#680243335126056988> on how to verify yourself, and then read <#680225381793529885> afterward.*";

		HallRoboticsBot.getConfig().getWelcomeChannel().sendMessage(new MessageBuilder(botMsg).build()).queue();
	}

}
